# frozen_string_literal: true
# !/usr/bin/env ruby

Dado("que queira realizar um cadastro no SignTogo") do
   @home = Home.new
   @home.load
end

Quando("inserir as informações solicitadas e aceitar os Termos de Uso") do
  cadastro = CreateAccount.new
  cadastro.create_account
end

Quando("preencher a segunda etapa") do
   cadsegunda = CreateAccountPt.new
   cadsegunda.createaccount_pt
end

Então("a primeira etapa do cadastro será feita e a seguinte mensagem será exibida {string}") do |text|
   sleep 5
   assert_text(text)
end

