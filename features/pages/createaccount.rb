class CreateAccount < SitePrism::Page

  element :nome, 'div[data-test-id="name"] input'
  element :sobrenome, 'div[data-test-id="lastname"] input'
  element :email, 'div[data-test-id="email"] input'
  element :telefone, 'div[data-test-id="phone"] input'
  element :pais, 'div[data-test-id="country"] select option[value="31"]'
  element :ramo, 'div[data-test-id="sector-company"] select option[value="17"]'
  element :senha, 'div[data-test-id="password"] input'
  element :submit, 'button[data-test-id="button-register"]'
  element :aceitar, 'div[data-test-id="accept-terms"] span'
  element :continuar, 'button[data-test-id="button-continue"]'

  


def create_account
    sleep 6
    information
    submit.click
    aceitar.click
    continuar.click
end

  private

  def information
    nome.set(Faker::Name.first_name)
    sobrenome.set(Faker::Name.last_name)
    email.set(Faker::Internet.email)
    telefone.set(11111111111)
    pais.click
    ramo.click
    senha.set(1234)
  end

end
  
     