require 'capybara/dsl'
require 'cucumber'
require 'faker'
require 'httparty'
require 'pry'
require 'rspec'
require 'selenium-webdriver'
require 'site_prism'

World Capybara::DSL

BROWSER = ENV['BROWSER'] || 'chrome'
HEADLESS = ENV['HEADLESS'] || true

Capybara.register_driver :selenium do |app|
  if BROWSER.eql?('firefox') && !HEADLESS

    capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
      firefoxOptions: { args: %w[window-size=1366,768] }
    )
    Capybara::Selenium::Driver.new(app, browser: :chrome, desired_capabilities: capabilities)

  #elsif BROWSER.eql?('chrome') && HEADLESS
    #capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
      #chromeOptions: { args: %w[headless disable-gpu no-sandbox window-size=1366,768] }
    #)
    #Capybara::Selenium::Driver.new(app, browser: :chrome, desired_capabilities: capabilities)

  elsif BROWSER.eql?('firefox') && HEADLESS
    firefox_options = Selenium::WebDriver::Firefox::Options.new
    firefox_options.add_argument('--headless')
    firefox_options.add_argument('--width=1900')
    firefox_options.add_argument('--height=1080')
    Capybara::Selenium::Driver.new(app, browser: :firefox, options: firefox_options)

   elsif BROWSER.eql?('chrome') && HEADLESS
    chrome_options = Selenium::WebDriver::Chrome::Options.new
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument("--lang=pt-PT")
    chrome_options.add_argument('--width=1900')
    chrome_options.add_argument('--height=1080')
    #driver = webdriver.Chrome('/usr/bin/chromedriver',chrome_options=chrome_options)
    Capybara::Selenium::Driver.new(app, browser: :chrome, options: chrome_options)

  end
end

Capybara.configure do |config|
  Capybara.current_driver = :selenium
  config.default_max_wait_time = 5
end
