# language: pt

Funcionalidade: Cadastrar Novo Usuário

Cenario: Cadastrar novo usuário com sucesso primeira etapa
Dado que queira realizar um cadastro no SignTogo
Quando inserir as informações solicitadas e aceitar os Termos de Uso
E preencher a segunda etapa
Então a primeira etapa do cadastro será feita e a seguinte mensagem será exibida "Acesse sua caixa de e-mail"

